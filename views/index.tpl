<!DOCTYPE html>

<html>
<head>
  <title>Beego</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style type="text/css">
    *,body {
      margin: 0px;
      padding: 0px;
    }

    body {
      margin: 0px;
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 20px;
      background-color: #fff;
    }

    header,
    footer {
      width: 960px;
      margin-left: auto;
      margin-right: auto;
    }

    .logo {
      background-image: url('/static/img/gophers.jpg');
      background-repeat: no-repeat;
      -webkit-background-size: 100px 100px;
      background-size: 130px 130px;
      background-position: center center;
      text-align: center;
      font-size: 42px;
      padding: 250px 0 70px;
      font-weight: normal;
      text-shadow: 0px 1px 2px #ddd;
    }

    header {
      padding: 100px 0;
    }

    footer {
      line-height: 1.8;
      text-align: center;
      padding: 50px 0;
      color: #999;
    }

    .description {
      text-align: center;
      font-size: 16px;
      text-shadow: 0px 1px 2px #ddd;
    }

    a {
      color: #444;
      text-decoration: none;
    }

    .backdrop {
      position: absolute;
      background-image: url('/static/img/starlight_black.jpg');
      background-size: cover;
      width: 100%;
      height: 100%;
      box-shadow: inset 0px 0px 100px #ddd;
      z-index: -1;
      top: 0px;
      left: 0px;
    }
  </style>
  <script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>
</head>

<body>
  <header>
    <h1 class="logo">Welcome to Beego</h1>
    <div class="description">
      Beego is a simple & powerful Go web framework which is inspired by tornado and sinatra.
    </div>
  </header>
  <content>
    &nbsp;
    {{if .flash.error}}
    <h3>{{.flash.error}}</h3>
    &nbsp;
    {{end}}{{if .flash.notice}}
    <h3>{{.flash.notice}}</h3>
    &nbsp;
    {{end}}
    {{if .Errors}}
    {{range $rec := .Errors}}
    <h3>{{$rec}}</h3>
    {{end}}
    &nbsp;
    {{end}}
    <form method="POST">
      <table>
        <tr>
          <td>Create n new users:</td>
          <td><input name="nusers" type="txt" autofocus /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td><td><input type="submit" value="Create" /></td><td><a href="http://localhost:{{.Httpport}}/">Create Users</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Search user by name:</td>
          <td><input name="suser" type="txt" autofocus /></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <td>&nbsp;</td><td><input type="submit" value="Search" /></td><td><a href="http://localhost:{{.Httpport}}/">Search User</a></td>
        </tr>
      </table>
    </form>
  </content>
  <footer>
    <div class="author">
      Official website:
      <a href="http://{{.Website}}">{{.Website}}</a> /
      Contact me:
      <a class="email" href="mailto:{{.Email}}">{{.Email}}</a>
    </div>
  </footer>
  <div class="backdrop"></div>
  <script src="/static/js/reload.min.js"></script>
</body>
</html>

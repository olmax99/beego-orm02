package models

import (
	"time"
)

type Orm02User struct {
	Id      int
	Name    string
	Email   string        `orm:"unique"`
	Profile *Orm02Profile `orm:"rel(one)"` // OneToOne relation
}

type Orm02Profile struct {
	Id      int
	Age     int16
	Answer  string
	Created time.Time  `orm:"type(datetime);precision(2)"`
	Balance float64    `orm:"digits(12);decimals(4)"`
	User    *Orm02User `orm:"reverse(one)"` // Reverse relationship (optional)
	AnyT    time.Time  `orm:"null"`
}

func init() {}

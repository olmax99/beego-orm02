module beego-orm02

go 1.16

require github.com/beego/beego/v2 v2.0.1

require (
	github.com/astaxie/beego v1.12.3
	github.com/goombaio/namegenerator v0.0.0-20181006234301-989e774b106e
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/smartystreets/goconvey v1.6.4
)

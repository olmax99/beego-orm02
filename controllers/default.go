package controllers

import (
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"

	"beego-orm02/models"

	"github.com/astaxie/beego/validation"
	"github.com/beego/beego/v2/adapter/orm"
	"github.com/beego/beego/v2/core/utils"
	beego "github.com/beego/beego/v2/server/web"
	"github.com/goombaio/namegenerator"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Index() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"

	p := utils.RandomCreateBytes(16)
	log.Printf("INFO [+] RandomCreateBytes() .. %v", string(p))

	dbalias, err := beego.AppConfig.String("db::beego_db_alias")
	if err != nil {
		fmt.Printf("ERROR [*] Index.dbalias.. %v", err)
	}
	log.Printf("INFO [*] Using DB alias: %v", dbalias)

	// Take form input for generating users in DB
	if c.Ctx.Input.Method() == "POST" {
		nusers := c.GetString("nusers")
		suser := c.GetString("suser")

		if nusers != "" {
			log.Printf("INFO [*] Tnusers: %T", nusers)
			valid := validation.Validation{}
			valid.Numeric(nusers, "nusers")
			if valid.HasErrors() {
				errmap := []string{}
				for _, err := range valid.Errors {
					errmap = append(errmap, "validation failed: "+err.Key+":"+err.Message)
				}
				log.Printf("ERROR [*] validation.. %v", errmap)
			}
			iusers, err := strconv.Atoi(nusers)
			if err != nil {
				log.Printf("ERROR [*] strconv.. %v", err)
			}
			log.Printf("INFO [+] Create: %v, T(%T)", iusers, iusers)
			// Create users
			o := orm.NewOrm()
			o.Using(dbalias)

			errO := fillTable(iusers, o)
			if errO != nil {
				log.Printf("ERROR [*] fillTable.. %v", errO)
			}

			// -------- Unrelated reading attempt (empty time)------------
			// return first user
			// Question: How can empty time.Time object be tested on?
			var profile models.Orm02Profile
			errW := o.QueryTable("orm02_profile").Filter("User__Id", 1).One(&profile)
			if errW == nil {
				fmt.Println(profile)
			}
			// Answer:
			if profile.AnyT.Equal(time.Time{}) {
				log.Printf("INFO [*] Yeaaap..it's empty")
			}
			if !profile.AnyT.Equal(time.Time{}) {
				log.Printf("INFO [*] Nooope..it's not empty")
			}
			log.Printf("INFO [+] Query: %#v", profile)
			log.Printf("INFO [+] Query: %+v", profile.AnyT)

		}

		if suser != "" {
			u := c.ReadOrm02User(suser, dbalias)()
			err := DoStuffWithUser(u)
			if err != nil {
				log.Printf("INFO [*] DoStuff.. %v", err)
			}
		}
	}
	errR := c.Render()
	if errR != nil {
		fmt.Println(errR)
	}
}

func (c *MainController) ReadOrm02User(se, al string) func() *models.Orm02User {
	flash := beego.NewFlash()
	o := orm.NewOrm()
	o.Using(al)
	user := &models.Orm02User{Name: se}
	fc := func() *models.Orm02User {
		err := o.Read(user, "Name")
		if err != nil {
			switch errM := err.Error(); {
			case errM == orm.ErrNoRows.Error():
				log.Printf("ERROR [*] No result found.. %v", err)
				flash.Error("No such user/email")
				flash.Store(&c.Controller)
				errR := c.Render()
				if errR != nil {
					fmt.Println(errR)
				}
			case errM == orm.ErrMissPK.Error():
				log.Printf("ERROR [*] No primary key found.. %v", err)
				flash.Error("No such user/email")
				flash.Store(&c.Controller)
				errR := c.Render()
				if errR != nil {
					fmt.Println(errR)
				}
			default:
				log.Printf("ERROR [*] Something else went wrong.. %v", err)
				flash.Error("No such user/email")
				flash.Store(&c.Controller)
				errR := c.Render()
				if errR != nil {
					fmt.Println(errR)
				}
			}
		}
		return user
	}
	return fc
}

func DoStuffWithUser(u *models.Orm02User) error {
	log.Printf("INFO [*] do something with user: %#v", u)
	return nil
}

func fillTable(num int, db orm.Ormer) error {
	answers := []string{
		"It is certain",
		"It is decidedly so",
		"Without a doubt",
		"Yes definitely",
		"You may rely on it",
		"As I see it yes",
		"Most likely",
		"Outlook good",
		"Yes",
		"Signs point to yes",
		"Reply hazy try again",
		"Ask again later",
		"Better not tell you now",
		"Cannot predict now",
		"Concentrate and ask again",
		"Don't count on it",
		"My reply is no",
		"My sources say no",
		"Outlook not so good",
		"Very doubtful",
	}
	emails := []string{
		"yahoo.com",
		"gmail.com",
		"yahoo.com",
		"hotmail.com",
		"aol.com",
		"hotmail.co.uk",
		"hotmail.fr",
		"msn.com",
		"yahoo.fr",
		"wanadoo.fr",
		"orange.fr",
		"comcast.net",
		"yahoo.co.uk",
		"yahoo.com.br",
		"yahoo.co.in",
		"live.com",
		"rediffmail.com",
		"free.fr",
		"gmx.de",
	}
	balances := randFloats(0.0000, 101.9899, 300)
	errmapP := make(map[int]error)
	errmapU := make(map[int]error)
	for i := 0; i < num; i++ {
		log.Printf("INFO [+] Add ID('%v')", i)
		// helpers
		seed := time.Now().UTC().UnixNano()
		min := 10
		max := 80
		age := int16(rand.Intn(max-min) + min)
		create := randate()
		nameGenerator := namegenerator.NewNameGenerator(seed)
		name := nameGenerator.Generate()

		profile := new(models.Orm02Profile)
		profile.Age = age
		profile.Answer = answers[rand.Intn(len(answers))]
		profile.Balance = balances[rand.Intn(len(balances))]
		profile.Created = create

		user := new(models.Orm02User)
		user.Profile = profile
		user.Name = name
		user.Email = name + "@" + emails[rand.Intn(len(emails))]

		_, errP := db.Insert(profile)
		if errP != nil {
			log.Printf("WARNING [*] InsertP.. %v", errP)
			errmapP[i] = errP
		}
		_, errU := db.Insert(user)
		if errU != nil {
			log.Printf("WARNING [*] InsertU.. %v", errU)
			errmapU[i] = errU
		}
	}
	if len(errmapU) > 0 {
		return errmapU[0]
	}
	log.Println("INFO [+] success.")
	return nil

}

// return a random time.Time within a pre-defined period
func randate() time.Time {
	min := time.Date(2015, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2021, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}

// return list of len n with random floats
func randFloats(min, max float64, n int) []float64 {
	res := make([]float64, n)
	for i := range res {
		res[i] = min + rand.Float64()*(max-min)
	}
	return res
}

package main

import (
	"beego-orm02/models"
	_ "beego-orm02/routers"
	"fmt"
	"os"
	"strings"

	"github.com/beego/beego/v2/adapter/orm"
	beego "github.com/beego/beego/v2/server/web"

	_ "github.com/mattn/go-sqlite3"
)

// returns map of beego configs, i.e. "db::beego_db_alias" -> m["beego_db_alias"] = Value
func beeConf(ini ...string) map[string]string {
	m := make(map[string]string, len(ini))
	for _, i := range ini[:] {
		entry, err := beego.AppConfig.String(i)
		if err != nil {
			fmt.Printf("ERROR [*] beeConf().. %v", err)
		}
		s := strings.Split(i, "::")
		m[s[1]] = entry
	}
	return m
}

func init() {
	dbalias, err := beego.AppConfig.String("db::beego_db_alias")
	if err != nil {
		fmt.Println(err)
	}

	os.MkdirAll("./data/dev/", 0755)

	orm.RegisterDriver("sqlite3", orm.DRSqlite)
	orm.RegisterDataBase("default", "sqlite3", "./data/dev/default.db")
	if dbalias != "default" {
		orm.RegisterDataBase(dbalias, "sqlite3", "./data/dev/"+dbalias+".db")
	}
	orm.RegisterModel(new(models.Orm02User), new(models.Orm02Profile))
}

func main() {
	force := false
	verbose := false
	beeC := beeConf(
		"db::beego_db_alias",
		"db::beego_db_bootstrap",
		"db::beego_db_debug",
	)
	if beeC["beego_db_debug"] == "true" {
		orm.Debug = true
	}
	if beeC["beego_db_bootstrap"] == "true" {
		force = true
		verbose = true
	}
	name := beeC["beego_db_alias"]
	err := orm.RunSyncdb(name, force, verbose)
	if err != nil {
		fmt.Println(err)
	}
	orm.RunCommand()
	beego.Run()
}

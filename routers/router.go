package routers

import (
	"beego-orm02/controllers"

	beego "github.com/beego/beego/v2/server/web"
)

func init() {
	beego.Router("/", &controllers.MainController{}, "get,post:Index")
}
